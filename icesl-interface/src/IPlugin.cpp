/*
 *  Copyright (c) 2023, Inria
 *  All rights reserved.
 *
 *  IceSL plugin interface
 *
 *  MFX Team
 */

// SP 2020-09-23

#include "IPlugin.h"

const std::string IceSLInterface::InterfaceVersion = "0.13";

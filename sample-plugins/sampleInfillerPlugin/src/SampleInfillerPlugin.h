#pragma once

#include "IInfillerPlugin.h"

// -----------------------------------------------

extern "C" ICESL_PLUGIN_API IceSLInterface::IPlugin * createPlugin();

extern "C" ICESL_PLUGIN_API void destroyPlugin(IceSLInterface::IPlugin* plugin);

// -----------------------------------------------

class SampleInfiller : public IceSLInterface::IInfillerInterface
{
public:

  SampleInfiller() = default;
  virtual ~SampleInfiller() override = default;

  void setNumSlices(int num) override;
  void setCoordSpaces(
    v3f processing_world_corner_mm,
    v3f processing_extent_mm,
    v3f tight_world_corner_mm,
    v3f tight_extent_mm,
    v3f toolpaths_world_corner_mm,
    v3f toolpaths_extent_mm) override;

  // called once at start
  void prepareInfill(int brush) override;

  // called once at end
  void terminateInfill(int brush) override;

  // called before each new slice
  void prepareInfillForSlice(int id, const AAB<2, int> &xy_slice_box, float layer_height_mm, double layer_thickness_mm, int brush) override;

  // called to generate the infill in a surface
  bool generateInfill(
    int id,
    float layer_height_mm,
    double layer_thickness_mm,
    int brush,
    const ClipperLib::Paths &surface,
    std::vector<std::unique_ptr<IceSLInterface::IPath> > &fills,
    bool &preserve_order,
    ClipperLib::Paths &_fallback_surface) override;

private:
  // transforms processing space coordinates (clipper points) into build space (machine language coordinates)
  std::function<v2f(ClipperLib::IntPoint)> m_xyBuildSpaceFnc;

};

class SampleInfillerPlugin : public IceSLInterface::IInfillerPlugin
{
public:

  using IInfillerInterface = IceSLInterface::IInfillerInterface;

  SampleInfillerPlugin() = default;
  virtual ~SampleInfillerPlugin() = default;

  bool initialize(IceSLInterface::IPluginEnvironment& env) override
  {
    IInfillerPlugin::initialize(env);
    ImGui::SetCurrentContext(env.getImGuiContext());
    gluxInit();
    return true;
  }

  void dispose() override {}

  void gui(bool postService) override {}

  std::string name()    const override { return "SampleInfiller"; }
  std::string author()  const override { return "Cédric Zanni, Salim Perchy and Sylvain Lefebvre"; }
  std::string comment() const override { return "Example of an infiller plugin"; }

  bool addPluginSettings(IceSLInterface::EnumerableSettingsInterface& enumerable_settings) override;

  std::unique_ptr<IInfillerInterface> createInfiller() override
  {
     return std::unique_ptr<IInfillerInterface>(new SampleInfiller());
  }

private:

  std::vector<float> m_LineWidth_mm;
  float m_LineWidth_min_mm = 0.01f;
  float m_LineWidth_max_mm = 1.0f;
};

// -----------------------------------------------
